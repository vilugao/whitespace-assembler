; Whitespace assembly translated from example at
; <http://compsoc.dur.ac.uk/whitespace/tutorial.html>.
;
; This is a program which counts from 1 to 10, outputting the current
; value as it goes.

	push 1
'C:
	dup
	putn
	push '\n
	putc
	push 1
	add
	dup
	push 11
	sub
	jz 'E
	jmp 'C
'E:
	pop
	end
