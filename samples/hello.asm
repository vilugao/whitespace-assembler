; Hello world sample code taken from
; <https://en.wikipedia.org/wiki/Whitespace_(programming_language)#Sample_code>

push 'H
putc
push 'e
putc
push 'l
putc
push 'l
putc
push 'o
putc
push ',
putc
push 32
putc
push 'w
putc
push 'o
putc
push 'r
putc
push 'l
putc
push 'd
putc
push '!
putc
end
