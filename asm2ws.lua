#!/usr/bin/env lua
local keywords = {
	-- Stack manipulation
	["push"] = "ss",
	["dup"] = "sls",
	["swap"] = "slt",
	["pop"] = "sll",
	-- Arithmetic
	["add"] = "tsss",
	["sub"] = "tsst",
	["mul"] = "tssl",
	["div"] = "tsts",
	["mod"] = "tstt",
	-- Heap access
	["stor"] = "tts",
	["retr"] = "ttt",
	-- Flow control
	["lbl"] = "lss",
	["call"] = "lst",
	["jmp"] = "lsl",
	["jz"] = "lts",
	["js"] = "ltt", -- <- Deprecated. Use the `jneg` or `jl` opcode instead.
	["jneg"] = "ltt", -- <- Better alternative over `js` opcode.
	["jl"] = "ltt", -- <- Better alternative over `js` opcode.
	["ret"] = "ltl",
	["end"] = "lll",
	-- Input/output
	["putc"] = "tlss",
	["putn"] = "tlst",
	["getc"] = "tlts",
	["getn"] = "tltt",
}

local function dec2bin(dec)
	dec = tonumber(dec, 10)
	local bin = "0"

	if dec < 0 then
		bin = "1"
		dec = -dec
	end

	while dec > 0 do
		bin = (dec % 2) .. bin
		dec = math.floor(dec / 2)
	end

	return bin:sub(-1, -1) .. bin:sub(1, -2)
end

local function print_whitespace(s)
	s = s:gsub("[stl01]", {
		["s"] = " ", ["0"] = " ",
		["t"] = "\t", ["1"] = "\t",
		["l"] = "\n",
	})
	io.write(s)
end

function main(arg)
	local flags = {}

	for _, argv in ipairs(arg) do
		if flags.nextarg then
			flags[flags.nextarg] = argv
			flags.nextarg = nil
		elseif argv == "--help" or argv:find("^[-/][h?]$") ~= nil then
			print(string.format(
				"Usage: %s [[-i] FILE] [-o OUTFILE]",
				table.concat(arg, " ", -1, 0)))
			return
		elseif argv == "-o" then
			flags.nextarg = "fileoutput"
		elseif argv:find("^[-]o.+$") ~= nil then
			flags.fileoutput = argv:match("^[-]o(.+)$")
		elseif argv == "-i" then
			flags.nextarg = "fileinput"
		elseif argv:find("^[-]i.+$") ~= nil then
			flags.fileinput = argv:match("^[-]i(.+)$")
		else
			flags.fileinput = argv
		end
	end

	io.output(flags.fileoutput)

	for l in io.lines(flags.fileinput) do
		-- Strip comments
		l = l:gsub(";.*$", "")

		for token in l:gmatch("%S+") do
			if keywords[token:lower()] ~= nil then
				-- Keyword
				print_whitespace(keywords[token])

			elseif token:find("^[+-]?%d+:?$") ~= nil then
				token = { token:match("^([+-]?%d+)(:?)$") }
				-- Number labeled
				if token[2] == ":" then
					print_whitespace(keywords["lbl"])
				end
				-- Number
				print_whitespace(dec2bin(token[1]) .. "l")

			elseif token:find("^'\\?%S:?$") ~= nil then
				token = { token:match("^'(\\?)(%S)(:?)$") }
				-- Char labeled
				if token[3] == ":" then
					print_whitespace(keywords["lbl"])
				end
				-- Char
				if token[1] == "\\" then
					if token[2] == "0" then
						token = 0
					else
						token = ({
							["a"] = 7,
							["b"] = 8,
							["t"] = 9,
							["n"] = 10,
							["v"] = 11,
							["f"] = 12,
							["r"] = 13,
						})[token[2]] or token[2]:byte()
					end
				else
					token = token[2]:byte()
				end
				print_whitespace(dec2bin(token) .. "l")

			else
				-- Unknown
				error(string.format(
					"Token `%s' desconhecido", token))
			end
		end
	end
end

main(arg)
