# Whitespace assembler

A program written in the Lua programming language
that compiles Whitespace assembly into Whitespace.
Made for ease of coding for Whitespace programming language.

## Whitespace assembly file format

An Assembly file for Whitespace should have a format
that looks like the Assembly programming language,
that is, one instruction per line.

### Numbers and characters

The placeholder `<number>` as a parameter can be
integers (positive or negative) or characters.

The characters must be prefixed by `'` (ASCII apostrophe)
and only the alphanumeric or punctuation ASCII characters are accepted,
except the semicolon character `;` (ASCII code 59),
which is reserved for comments.
The special characters
`'\0`, `'\a`, `'\b`, `'\t`, `'\n`, `'\v`, `'\f` and `'\r`
respectively represents the null, bell, backspace, horizontal tab, new line,
vertical tab, form feed, and carriage return characters.

Integer examples: `123`, `-45`, `+67`.
Character examples: `'A`, `'a`, `'1`.

### Comments

Line comments start with `;` (semicolon).

### Stack manipulation <sup>[1][ws-tutor]</sup>

Stack manipulation is one of the more common operations.
There are four stack instructions.

 - `push <number>`: Push the number onto the stack.
 - `dup`: Duplicate the top item on the stack.
 - `swap`: Swap the top two items on the stack.
 - `pop`: Discard the top item on the stack.

### Arithmetic <sup>[1][ws-tutor]</sup>

Arithmetic commands operate on the top two items on the stack,
and replace them with the result of the operation.
The first item pushed is considered to be left of the operator.

 - `add`: Addition.
 - `sub`: Subtraction.
 - `mul`: Multiplication.
 - `div`: Integer Division.
 - `mod`: Modulo.

### Heap access <sup>[1][ws-tutor]</sup>

Heap access commands look at the stack to find
the address of items to be stored or retrieved.
To store an item, push the address then the value and run the store command.
To retrieve an item, push the address and run the retrieve command,
which will place the value stored in the location at the top of the stack.

 - `stor`: Store.
 - `retr`: Retrieve.

### Flow Control <sup>[1][ws-tutor]</sup>

Flow control operations are also common.
Subroutines are marked by labels,
as well as the targets of conditional and unconditional jumps,
by which loops can be implemented.
Programs must be ended by means of `end`
so that the interpreter can exit cleanly.

 - `lbl <number>` or `<number>:`: Mark a location in the program.
 - `call <number>`: Call a subroutine.
 - `jmp <number>`: Jump unconditionally to a label.
 - `jz <number>`: Jump to a label if the top of the stack is zero.
 - `jneg <number>` or `jl <number>`: Jump to a label
	if the top of the stack is negative (less than zero).
 - `ret`: End a subroutine and transfer control back to the caller.
 - `end`: End the program.

### Input and output <sup>[1][ws-tutor]</sup>

Finally, we need to be able to interact with the user.
There are IO instructions
for reading and writing numbers and individual characters.
With these, string manipulation routines can be written.

The *read* instructions take the heap address
in which to store the result from the top of the stack.

 - `putc`: Write a character.
 - `putn`: Write a number.
 - `getc`: Read a character.
 - `getn`: Read a number.

### Examples

See examples in directory [samples/](samples/).

## How to compile

Open your shell terminal and run the following command format:

	lua asm2ws.lua [[-i] FILE] [-o OUTFILE]

Example: compile `hello.asm` into Whitespace called `hello.ws`:

	$ lua asm2ws.lua samples/hello.asm -o hello.ws

## References

 1. Whitespace tutorial.
	<[http://compsoc.dur.ac.uk/whitespace/tutorial.html][ws-tutor]>.

 [ws-tutor]: <http://compsoc.dur.ac.uk/whitespace/tutorial.html> "Whitespace tutorial"
